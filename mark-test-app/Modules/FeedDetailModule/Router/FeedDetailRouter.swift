//
//  FeedDetailRouter.swift
//  mark-test-app
//
//  Created mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit
import SafariServices

class FeedDetailRouter: FeedDetailRouterProtocol {
    weak var view: UIViewController!
}

// View -> Router

extension FeedDetailRouter {
    
    func openLinkInBrowser(url: URL) {
        let safariVC = SFSafariViewController(url: url)
        safariVC.navigationController?.navigationBar.isTranslucent = false
        safariVC.navigationController?.toolbar.isTranslucent = false
        self.view.present(safariVC, animated: true, completion: nil)
    }
    
}

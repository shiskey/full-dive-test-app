//
//  FeedDetailPresenter.swift
//  mark-test-app
//
//  Created mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedDetailPresenter: FeedDetailPresenterProtocol {
    private var interactor: FeedDetailInteractorProtocol
    private var router: FeedDetailRouterProtocol
    weak var view: FeedDetailViewProtocol!
    
    init(view: FeedDetailViewProtocol, interactor: FeedDetailInteractorProtocol, router: FeedDetailRouterProtocol) {
        self.interactor = interactor
        self.router = router
        self.view = view
    }
    
}

// MARK: View -> Presenter

extension FeedDetailPresenter {
    func openLink(with url: URL) {
        self.router.openLinkInBrowser(url: url)
    }
}

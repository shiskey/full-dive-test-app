//
//  FeedDetailProtocols.swift
//  mark-test-app
//
//  Created mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

// MARK: - View
protocol FeedDetailViewProtocol: class {
    var presenter: FeedDetailPresenterProtocol? { get set }
    var feed: FeedItem? { get set }
}

// MARK: - Presenter
protocol FeedDetailPresenterProtocol: class {
    // View -> Presenter
    func openLink(with url: URL)
}

// MARK: - Interactor
protocol FeedDetailInteractorProtocol: class {
    
}

// MARK: - Router
protocol FeedDetailRouterProtocol: class {
    // Presenter -> Router
    func openLinkInBrowser(url: URL)
}

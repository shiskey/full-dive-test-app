//
//  FeedDetailAssembly.swift
//  mark-test-app
//
//  Created mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedDetailAssembly {
    
    func build(with feed: FeedItem) -> UIViewController {
        let view = FeedDetailViewController(nibName: "FeedDetailViewController", bundle: nil)
        let interactor = FeedDetailInteractor()
        let router = FeedDetailRouter()
        let presenter = FeedDetailPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        view.feed = feed
        interactor.presenter = presenter
        router.view = view
        
        return view
    }
    
}

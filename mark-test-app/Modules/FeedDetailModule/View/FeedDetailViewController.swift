//
//  FeedDetailViewController.swift
//  mark-test-app
//
//  Created mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedDetailViewController: UIViewController, FeedDetailViewProtocol {
    var presenter: FeedDetailPresenterProtocol?
    var feed: FeedItem?
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bookmarkButton: UIButton!
    @IBOutlet var browserButton: UIButton!
    
    private var isBookmarked: Bool? {
        didSet {
            if let isBookmarked = isBookmarked {
                let bookmarkTitle = isBookmarked
                    ? "Удалить из закладок" : "Добавить в закладки"
                self.bookmarkButton.setTitle(bookmarkTitle, for: .normal)
            } else {
                self.bookmarkButton.isHidden = true
            }
        }
    }
    private var titleString: String? {
        didSet {
            if let titleString = titleString {
                self.titleLabel.text = titleString
            } else {
                self.titleLabel.isHidden = true
            }
        }
    }
    private var imageUrl: URL? {
        didSet {
            if let imageUrl = imageUrl {
                self.imageView.load(from: imageUrl)
            } else {
                self.imageView.isHidden = true
            }
        }
    }
}

extension FeedDetailViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup self
        self.navigationItem.title = feed?.feedName

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // setup elements
        self.titleString = self.feed?.title
        self.imageUrl = self.feed?.imageUrl
        self.isBookmarked = self.feed?.isBookmarked
    }
    
    @IBAction func didTapButton(_ button: UIButton) {
        switch button {
        case self.bookmarkButton:
            self.feed?.toggleBookmark()
            self.isBookmarked = self.feed?.isBookmarked
        case self.browserButton:
            if let url = self.feed?.linkUrl {
                self.presenter?.openLink(with: url)
            }
        default:
            break
        }
    }
    
}

//
//  BookmarksModuleProtocols.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

// MARK: - View
protocol BookmarksModuleViewProtocol: class {
    var items: [FeedItem] { get set }
    // Presenter -> View
    func reloadItems(with items: [FeedItem])
    func showError(with string: String)
}

// MARK: - Presenter
protocol BookmarksModulePresenterProtocol: class {
    // View -> Presenter
    func viewDidAppear()
    func showFeedDetail(for feed: FeedItem)
    // Interactor -> Presenter
    func didRetriveFeeds(feeds: [FeedItem])
    func didFailRetrive(with errorMessage: String)
}

// MARK: - Interactor
protocol BookmarksModuleInteractorProtocol: class {
    // Presenter -> Interactor
    func retrieveFeeds()
}

// MARK: - Router
protocol BookmarksModuleRouterProtocol: class {
    // Presenter -> Router
    func presentFeedDetail(for feed: FeedItem)
}

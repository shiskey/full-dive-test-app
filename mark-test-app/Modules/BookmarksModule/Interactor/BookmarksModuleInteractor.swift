//
//  BookmarksModuleInteractor.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation
import RealmSwift

class BookmarksModuleInteractor: BookmarksModuleInteractorProtocol {
    weak var presenter: BookmarksModulePresenterProtocol!
}

// MARK: Presenter -> Interactor

extension BookmarksModuleInteractor {
    
    func retrieveFeeds() {
        guard let r = try? Realm() else {
            self.presenter.didFailRetrive(with: "Unable to retrive bookmarks")
            return
        }
        let bookmarkedFeeds = r.objects(FeedItem.self).filter { $0.isBookmarked }
        self.presenter.didRetriveFeeds(feeds: Array(bookmarkedFeeds))
    }
    
}

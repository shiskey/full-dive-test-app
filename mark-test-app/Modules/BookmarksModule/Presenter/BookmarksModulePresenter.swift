//
//  BookmarksModulePresenter.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class BookmarksModulePresenter: BookmarksModulePresenterProtocol {
    private var interactor: BookmarksModuleInteractorProtocol
    private var router: BookmarksModuleRouterProtocol
    weak var view: BookmarksModuleViewProtocol!
    
    init(view: BookmarksModuleViewProtocol, interactor: BookmarksModuleInteractorProtocol, router: BookmarksModuleRouterProtocol) {
        self.interactor = interactor
        self.router = router
        self.view = view
    }
    
}

// MARK: View -> Presenter

extension BookmarksModulePresenter {
    
    func viewDidAppear() {
        self.interactor.retrieveFeeds()
    }
    
    func showFeedDetail(for feed: FeedItem) {
        self.router.presentFeedDetail(for: feed)
    }
    
}

// MARK: Interactor -> Presenter

extension BookmarksModulePresenter {
    
    func didRetriveFeeds(feeds: [FeedItem]) {
        self.view.reloadItems(with: feeds)
    }
    
    func didFailRetrive(with errorMessage: String) {
        self.view.showError(with: errorMessage)
    }
    
}

//
//  BookmarksModuleRouter.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class BookmarksModuleRouter: BookmarksModuleRouterProtocol {
    weak var view: UIViewController!
}

// MARK: Presenter -> Router

extension BookmarksModuleRouter {
    func presentFeedDetail(for feed: FeedItem) {
        let feedDetailViewController = FeedDetailAssembly().build(with: feed)
        self.view.navigationController?.show(feedDetailViewController, sender: self)
    }
}

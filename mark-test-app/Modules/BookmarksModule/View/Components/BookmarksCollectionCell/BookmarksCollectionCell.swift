//
//  BookmarksCollectionCell.swift
//  mark-test-app
//
//  Created by mark on 03/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class BookmarksCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var title: String? {
        didSet {
            if let title = title {
                self.titleLabel.text = title
            } else {
                self.titleLabel.isHidden = true
            }
        }
    }
    private var imageUrl: URL? {
        didSet {
            if let imageUrl = imageUrl {
                self.feedImageView.isHidden = false
                self.feedImageView.load(from: imageUrl)
            } else {
                self.feedImageView.image = nil
                self.feedImageView.isHidden = true
            }
        }
    }
    
}

// MARK: Actions

extension BookmarksCollectionCell {
    
    func setup(with model: FeedItem) {
        self.title = model.title
        self.imageUrl = model.imageUrl
    }
    
}

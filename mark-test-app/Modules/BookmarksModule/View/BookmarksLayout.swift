//
//  BookmarksLayout.swift
//  mark-test-app
//
//  Created by mark on 03/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

protocol BookmarksLayoutDelagate: class {
    func collectionView(_ collectionView: UICollectionView,
                        heightForCellAtIndexPath indexPath: IndexPath) -> CGFloat
}

class BookmarksLayout: UICollectionViewLayout {
    
    weak var delegate: BookmarksLayoutDelagate?
    
    private var cache = [UICollectionViewLayoutAttributes]()
    private var numberOfColumns = 2
    private var padding: CGFloat = 6
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
        guard let collectionView = self.collectionView else { return 0 }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: self.contentWidth, height: self.contentHeight)
    }
    
    override func prepare() {
        super.prepare()
        
        self.cache.removeAll()
        
        guard self.cache.isEmpty == true, let collectionView = self.collectionView else {
            return
        }
        
        let columnWidth = self.contentWidth / CGFloat(self.numberOfColumns)
        var xOffset = [CGFloat]()
        
        for column in 0..<self.numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        
        var column = 0
        var yOffset = [CGFloat](repeating: 0, count: self.numberOfColumns)
        
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let photoHeight = self.delegate?.collectionView(collectionView, heightForCellAtIndexPath: indexPath)
            let height = self.padding * 2 + photoHeight!
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: self.padding, dy: self.padding)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            self.cache.append(attributes)
            
            self.contentHeight = max(self.contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            
            column = column < (self.numberOfColumns - 1) ? (column + 1) : 0
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in self.cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cache[indexPath.item]
    }
    
}

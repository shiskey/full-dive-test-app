//
//  BookmarksModuleViewController.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class BookmarksModuleViewController: UIViewController, BookmarksModuleViewProtocol {
    var presenter: BookmarksModulePresenterProtocol!
    var items: [FeedItem] = [FeedItem]()
    
    var collectionView: UICollectionView!
}

extension BookmarksModuleViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup self
        self.navigationItem.title = "Bookmarks"
        
        // setup collectionView
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: BookmarksLayout())
        self.collectionView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        (self.collectionView.collectionViewLayout as! BookmarksLayout).delegate = self
        self.view.addSubview(self.collectionView)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.collectionView.register(
            BookmarksCollectionCell.xib,
            forCellWithReuseIdentifier: BookmarksCollectionCell.reuseId
        )
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.viewDidAppear()
    }
    
}

// MARK: Presenter -> View

extension BookmarksModuleViewController {
    
    func reloadItems(with items: [FeedItem]) {
        let newItems = items.sorted { $0.date! > $1.date! }
        if self.items != newItems {
            self.items = newItems
            self.collectionView.reloadData()
        }
    }
    
    func showError(with string: String) {
        self.showAlert(with: string)
    }
}

// MARK: CollectionView delegate

extension BookmarksModuleViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: BookmarksCollectionCell.reuseId,
            for: indexPath
        ) as! BookmarksCollectionCell
        
        let cellModel = self.items[indexPath.row]
        cell.setup(with: cellModel)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let feed = self.items[indexPath.row]
        self.presenter.showFeedDetail(for: feed)    }
    
}

// MARK: BookmarksLayout delegate

extension BookmarksModuleViewController: BookmarksLayoutDelagate {
    func collectionView(_ collectionView: UICollectionView,
                        heightForCellAtIndexPath indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0.0
        let feed = self.items[indexPath.row]
        if feed.imageUrl != nil {
            height += 100.0
        }
        if feed.title != nil {
            height += 60.0
        }
        return height
    }
}


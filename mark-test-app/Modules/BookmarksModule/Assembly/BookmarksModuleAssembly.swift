//
//  BookmarksModuleAssembly.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class BookmarksModuleAssembly {
    
    func build() -> UIViewController {
        let wrappedView = BookmarksModuleViewController().wrap()
        let view = wrappedView.topViewController as! BookmarksModuleViewController
        let interactor = BookmarksModuleInteractor()
        let router = BookmarksModuleRouter()
        let presenter = BookmarksModulePresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        view.tabBarItem = UITabBarItem(
            title: "Bookmarks",
            image: UIImage(named: "star-icon"),
            selectedImage: UIImage(named: "star-icon")
        )
        interactor.presenter = presenter
        router.view = view
        
        return wrappedView
    }
    
}

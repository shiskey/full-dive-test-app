//
//  TabBarModuleAssembly.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class TabBarModuleAssembly {
    
    func build(with childViewControllers: [UIViewController]) -> UIViewController {
        let tabBarViewController = TabBarModuleViewController()
        let interactor = TabBarModuleInteractor()
        let router = TabBarModuleRouter()
        let presenter = TabBarModulePresenter(view: tabBarViewController, interactor: interactor, router: router)
        
        tabBarViewController.presenter = presenter
        childViewControllers.forEach {
            tabBarViewController.addChild($0)
        }
        interactor.presenter = presenter
        router.view = tabBarViewController
        
        return tabBarViewController
    }
    
}

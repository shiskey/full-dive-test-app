//
//  TabBarModulePresenter.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class TabBarModulePresenter: TabBarModulePresenterProtocol {
    private var interactor: TabBarModuleInteractorProtocol
    private var router: TabBarModuleRouterProtocol
    weak var view: TabBarModuleViewProtocol!
    
    init(view: TabBarModuleViewProtocol, interactor: TabBarModuleInteractorProtocol, router: TabBarModuleRouterProtocol) {
        self.interactor = interactor
        self.router = router
        self.view = view
    }
    
}

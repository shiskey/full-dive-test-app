//
//  TabBarModuleViewController.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class TabBarModuleViewController: UITabBarController, TabBarModuleViewProtocol {
    var presenter: TabBarModulePresenterProtocol!
}

//
//  TabBarModuleProtocols.swift
//  mark-test-app
//
//  Created mark on 02/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

// MARK: - View
protocol TabBarModuleViewProtocol: class {
    
}

// MARK: - Presenter
protocol TabBarModulePresenterProtocol: class {
    
}

// MARK: - Interactor
protocol TabBarModuleInteractorProtocol: class {
    
}

// MARK: - Router
protocol TabBarModuleRouterProtocol: class {
    
}

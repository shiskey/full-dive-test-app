//
//  FeedAssembly.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedAssembly {
    
    func build() -> UIViewController {
        let wrappedView = FeedViewController().wrap()
        let view = wrappedView.topViewController as! FeedViewController
        let interactor = FeedInteractor()
        let router = FeedRouter()
        let presenter = FeedPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        view.tabBarItem = UITabBarItem(
            title: "Feeds",
            image: UIImage(named: "feed-icon"),
            selectedImage: UIImage(named: "feed-icon")
        )
        interactor.presenter = presenter
        router.view = view
        
        return wrappedView
    }
    
}

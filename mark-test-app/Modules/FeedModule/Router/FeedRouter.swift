//
//  FeedRouter.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedRouter: FeedRouterProtocol {
    weak var view: UIViewController!
}

// MARK: Presenter -> Router

extension FeedRouter {
    
    func presentFeedDetail(for feed: FeedItem) {
        let feedDetailViewController = FeedDetailAssembly().build(with: feed)
        self.view.navigationController?.show(feedDetailViewController, sender: self)
    }
    
}

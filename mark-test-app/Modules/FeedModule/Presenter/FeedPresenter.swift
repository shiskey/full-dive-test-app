//
//  FeedPresenter.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

class FeedPresenter: FeedPresenterProtocol {
    private var interactor: FeedInteractorProtocol
    private var router: FeedRouterProtocol
    weak var view: FeedViewProtocol!
    
    init(view: FeedViewProtocol, interactor: FeedInteractorProtocol, router: FeedRouterProtocol) {
        self.interactor = interactor
        self.router = router
        self.view = view
    }
    
}

// MARK: View -> Presenter

extension FeedPresenter {
    
    func viewDidLoad() {
        self.interactor.retrieveFeeds()
    }
    
    func refreshFeeds() {
        self.interactor.retrieveFeeds()
    }
    
    func showFeedDetail(for feed: FeedItem) {
        self.router.presentFeedDetail(for: feed)
    }
    
}

// MARK: Interactor -> Presenter

extension FeedPresenter {
    
    func didRetriveFeeds(feeds: [FeedItem]) {
        self.view.reloadItems(with: feeds)
    }
    
    func didFailRetrive(with errorMessage: String) {
        self.view.showError(with: errorMessage)
    }
    
}

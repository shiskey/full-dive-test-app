//
//  FeedProtocols.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

// MARK: - View

protocol FeedViewProtocol: class {
    var items: [FeedItem] { get set }
    // Presenter -> View
    func reloadItems(with items: [FeedItem])
    func showError(with message: String)
}

// MARK: - Presenter

protocol FeedPresenterProtocol: class {
    // View -> Presenter
    func viewDidLoad()
    func refreshFeeds()
    func showFeedDetail(for feed: FeedItem)
    // Interactor -> Presenter
    func didRetriveFeeds(feeds: [FeedItem])
    func didFailRetrive(with errorMessage: String)
}

// MARK: - Interactor

protocol FeedInteractorProtocol: class {
    // Presenter -> Interactor
    func retrieveFeeds()
}

// MARK: - Router

protocol FeedRouterProtocol: class {
    // Presenter -> Router
    func presentFeedDetail(for feed: FeedItem)
}

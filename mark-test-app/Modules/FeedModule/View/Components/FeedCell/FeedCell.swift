//
//  FeedCell.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit
import Alamofire

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var feedNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var onBookmarkAction: (() -> ())?
    var isBookmarked: Bool = false {
        didSet {
            let image = isBookmarked
                ? UIImage(named: "bookmark_icon_fill")?.withRenderingMode(.alwaysTemplate)
                : UIImage(named: "bookmark_icon_stroke")?.withRenderingMode(.alwaysTemplate)
            self.bookmarkButton.setImage(image, for: .normal)
        }
    }
    
    private var title: String? {
        didSet {
            if let title = title {
                self.titleLabel.text = title
            } else {
                self.titleLabel.isHidden = true
            }
        }
    }
    private var feedName: String? {
        didSet {
            if let feedName = feedName {
                self.feedNameLabel.text = feedName
            } else {
                self.feedNameLabel.isHidden = true
            }
        }
    }
    private var date: Date? {
        didSet {
            if let date = date {
                self.dateLabel.text = date.stringDate
            } else {
                self.dateLabel.isHidden = true
            }
        }
    }
    private var imageUrl: URL? {
        didSet {
            if let imageUrl = imageUrl {
                self.feedImageView.isHidden = false
                self.feedImageView.load(from: imageUrl)
            } else {
                self.feedImageView.image = nil
                self.feedImageView.isHidden = true
            }
        }
    }

}

// MARK: Actions

extension FeedCell {
    
    func setup(with model: FeedItem, onBookmarkAction: (() -> ())?) {
        self.title = model.title
        self.feedName = model.feedName
        self.date = model.date
        self.imageUrl = model.imageUrl
        self.isBookmarked = model.isBookmarked
        self.onBookmarkAction = onBookmarkAction
    }
    
    @IBAction func bookmarkButtonAction() {
        self.onBookmarkAction?()
    }
    
}

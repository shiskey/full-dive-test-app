//
//  FeedViewController.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController, FeedViewProtocol {
    var presenter: FeedPresenterProtocol!
    var items: [FeedItem] = [FeedItem]()
}

// MARK: Common

extension FeedViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // setup self
        self.navigationItem.title = "Feeds"
        self.navigationController?.navigationBar.prefersLargeTitles = true

        // setup tableView
        self.tableView.register(FeedCell.xib, forCellReuseIdentifier: FeedCell.reuseId)
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        self.tableView.tableFooterView = UIView()
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)

        self.presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // update visible cells
        if let indexPath = self.tableView.indexPathsForVisibleRows {
            let models = indexPath.map { self.items[$0.row] }
            let visibleCells = self.tableView.visibleCells as! [FeedCell]
            for (i, c) in visibleCells.enumerated() {
                c.isBookmarked = models[i].isBookmarked
            }
        }
    }

}

// MARK: Actions

extension FeedViewController {
    
    @objc func pullToRefresh(_ refreshControl: UIRefreshControl) {
        self.presenter.refreshFeeds()
    }
    
}

// MARK: Presenter -> View

extension FeedViewController {

    func reloadItems(with items: [FeedItem]) {
        
        // end refreshing if needed
        if self.tableView.refreshControl!.isRefreshing {
            self.tableView.refreshControl?.endRefreshing()
            self.items.removeAll()
        }
        
        // append new items than reload
        self.items += items
        self.items.sort { $0.date! > $1.date! }
        self.tableView.reloadData()
    }

    func showError(with message: String) {
        self.showAlert(with: message)
    }

}

// MARK: TableView delegate

extension FeedViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.reuseId, for: indexPath) as! FeedCell
        let cellModel = self.items[indexPath.row]
        cell.setup(with: cellModel, onBookmarkAction: {
            cellModel.toggleBookmark()
            cell.isBookmarked = cellModel.isBookmarked
        })
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feed = self.items[indexPath.row]
        self.presenter.showFeedDetail(for: feed)
    }

}

//
//  FeedInteractor.swift
//  mark-test-app
//
//  Created mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation
import Alamofire
import FeedKit
import RealmSwift

class FeedInteractor: FeedInteractorProtocol {
    weak var presenter: FeedPresenterProtocol!
}

extension FeedInteractor {
    
    func retrieveFeeds() {
        for origin in Api.origins {
            Alamofire
                .request(origin, method: .get)
                .validate(statusCode: [200])
                .responseData { response in
                    switch response.result {
                    case .success(let data):
                        DispatchQueue(label: "realmQueue", qos: .userInitiated).async {
                            let parser = FeedParser(data: data)
                            let parsedFeed = parser.parse()
                            let feeds = parsedFeed.rssFeed?.items?.compactMap {
                                FeedItem.setup(with: parsedFeed.rssFeed?.title, feedItem: $0)
                            }
                            
                            guard let guids = feeds?.map({ $0.guid }) else {
                                DispatchQueue.main.async {
                                    self.presenter.didFailRetrive(with: """
                                    Unable to parse feeds of \(origin)
                                    """)
                                }
                                return
                            }
                            DispatchQueue.main.async {
                                let r = try! Realm()
                                let feeds = r.objects(FeedItem.self).filter { guids.contains($0.guid) }
                                self.presenter.didRetriveFeeds(feeds: Array(feeds) )
                            }
                        }
                    case .failure:
                        self.presenter.didFailRetrive(with: """
                        Unable to retrive feeds of \(origin)
                        """)
                    }
            }
        }
    }
    
}

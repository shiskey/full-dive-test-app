//
//  AppDelegate.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // setup Realm
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            deleteRealmIfMigrationNeeded: true
        )
        FeedItem.purge()
        
        // Setup initial viewController
        let feedViewController = FeedAssembly().build()
        let bookmarksViewController = BookmarksModuleAssembly().build()
        let rootViewController = TabBarModuleAssembly().build(
            with: [feedViewController, bookmarksViewController]
        )
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        
        return true
    }

}


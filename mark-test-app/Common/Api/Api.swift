//
//  Api.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

struct Api {
    static let origins = [
        "https://techcrunch.com/feed/",
        "https://lifehacker.com/rss",
        
        "https://news.un.org/feed/subscribe/en/news/all/rss.xml",
        "https://www.alexcurylo.com/blog/feed/",
    ]
}

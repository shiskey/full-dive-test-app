//
//  FeedItem.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import RealmSwift
import FeedKit
import SwiftSoup


class FeedItem: Object {
    
    @objc dynamic var guid: String = ""
    @objc dynamic var feedName: String?
    @objc dynamic var title: String?
    @objc dynamic var date: Date?
    @objc dynamic private var _imageUrlString: String?
    @objc dynamic private var _link: String?
    @objc dynamic var isBookmarked = false
    
    
    override static func primaryKey() -> String? {
        return "guid"
    }
    
    convenience init(with guid: String) {
        self.init()
        self.guid = guid
    }
    
}

// MARK: Computed properties

extension FeedItem {
    var imageUrl: URL? {
        return URL(string: _imageUrlString ?? "")
    }
    var linkUrl: URL? {
        return URL(string: _link ?? "")
    }
}

// MARK: Actions

extension FeedItem {
    func toggleBookmark() {
        if let r = try? Realm() {
            r.beginWrite()
            self.isBookmarked = !self.isBookmarked
            try! r.commitWrite()
        }
    }
    
    static func purge() {
        if let r = try? Realm() {
            let oldFeeds = r.objects(FeedItem.self).filter { !$0.isBookmarked }
            r.beginWrite()
            r.delete(oldFeeds)
            try! r.commitWrite()
        }
    }
    
    static func getOrCreate(with guid: String?) -> FeedItem {
        if let guid = guid {
            if let r = try? Realm(),
                let feed = r.object(ofType: FeedItem.self, forPrimaryKey: guid) {
                return feed
            }
            return FeedItem(with: guid)
        }
        return FeedItem()
    }
}

// MARK: Utils

extension FeedItem {
    
    static func setup(with rssName: String?, feedItem: RSSFeedItem) -> FeedItem {
        let r = try! Realm()
        let f = FeedItem.getOrCreate(with: feedItem.guid?.value)
        r.beginWrite()
        f.feedName = rssName
        f.title = feedItem.title
        f.date = feedItem.pubDate
        f._imageUrlString = extractFeedImage(from: feedItem.description)
        f._link = feedItem.link
        r.add(f, update: true)
        try! r.commitWrite()
        return f
    }
    
    private static func extractFeedImage(from htmlString: String?) -> String? {
        guard
            let htmlString = htmlString,
            let imgs = try? SwiftSoup.parse(htmlString).select("img"),
            let imgUrl = try? imgs.first()?.attr("src")
            else {
                return nil
        }
        return imgUrl
    }
    
}

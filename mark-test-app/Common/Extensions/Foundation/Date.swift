//
//  Date.swift
//  mark-test-app
//
//  Created by mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import Foundation

private let formatter = DateFormatter()

extension Date {
    var stringDate: String {
        formatter.setLocalizedDateFormatFromTemplate("d MMMM HH:mm")
        return formatter.string(from: self)
    }
}

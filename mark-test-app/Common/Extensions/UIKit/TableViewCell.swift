//
//  TableViewCell.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var xib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static var reuseId: String {
        return String(describing: self)
    }
    
}

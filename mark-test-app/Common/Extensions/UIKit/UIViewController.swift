//
//  UIViewController.swift
//  mark-test-app
//
//  Created by mark on 28/02/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func wrap() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
    
    func showAlert(with message: String) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

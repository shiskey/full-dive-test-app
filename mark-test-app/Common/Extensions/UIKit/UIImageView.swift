//
//  UIImageView.swift
//  mark-test-app
//
//  Created by mark on 01/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit
import Alamofire

extension UIImageView {
    
    func load(from imageUrl: URL) {
        self.image = nil
        Alamofire.request(imageUrl).responseData { response in
            switch response.result{
            case .success(let d):
                self.image = UIImage(data: d)
            case .failure(_):
                break
            }
        }
    }
    
}

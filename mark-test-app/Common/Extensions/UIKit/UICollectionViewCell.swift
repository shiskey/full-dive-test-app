//
//  UICollectionViewCell.swift
//  mark-test-app
//
//  Created by mark on 03/03/2019.
//  Copyright © 2019 Full dive. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    static var xib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static var reuseId: String {
        return String(describing: self)
    }
    
}
